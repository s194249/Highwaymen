import numpy as np
from matplotlib import pyplot as plt

with np.load('Value_Iteration_1000episodes.npz') as f:
    rewardsV, stepsV, parametersV = f['rewards'], f['steps'], f['parameters']

plt.figure(figsize=((4.5,3)))
plt.title('Accumulated Reward: Value Iteration')
plt.hist(rewardsV,bins=80,density=True,color='#69E5A7')
plt.xlabel('Reward')
plt.ylabel('Density')
plt.xticks(range(0, 41, 5))
plt.tight_layout()
plt.savefig('plots/reward_value.pdf')
plt.show()

plt.figure(figsize=((4.5,3)))
plt.title('No. of steps: Value Iteration')
plt.hist(stepsV,bins=80,density=True,color='#6AB8E6')
plt.xlabel('Number of steps')
plt.ylabel('Density')
plt.xticks(range(0, 41, 5))
plt.tight_layout()
plt.savefig('plots/steps_value.pdf')
plt.show()

with np.load('Q_agent_1000episodes.npz') as f:
    rewardsQ, stepsQ, parametersQ = f['rewards'], f['steps'], f['parameters']

plt.figure(figsize=((4.5,3)))
plt.title('Accumulated Reward: Q-Learning')
plt.hist(rewardsQ,bins=80,density=True,color='#69E5A7')
plt.xlabel('Reward')
plt.ylabel('Density')
plt.xticks(range(0, 41, 5))
plt.tight_layout()
plt.savefig('plots/reward_Q.pdf')
plt.show()

plt.figure(figsize=((4.5,3)))
plt.title('No. of steps: Q-Learning')
plt.hist(stepsQ,bins=80,density=True,color='#6AB8E6')
plt.xlabel('Number of steps')
plt.ylabel('Density')
plt.xticks(range(0, 41, 5))
plt.tight_layout()
plt.savefig('plots/steps_Q.pdf')
plt.show()

plt.figure(figsize=((4.5,3)))
plt.title('Rewards compared')
plt.plot(np.sort(rewardsV), color='#6AB8E6', label='Value Iteration')
plt.plot(np.sort(rewardsQ), color='#69E5A7', label='Q-Learning')
plt.ylabel('Reward')
plt.xlabel('Episode')
plt.legend()
plt.tight_layout()
plt.savefig('plots/sort_rewards.pdf')
plt.show()
