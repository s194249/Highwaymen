"""
This file may not be shared/redistributed without permission. Please read copyright notice in the git repo. If this file contains other copyright notices disregard this text.
"""
from collections import defaultdict
import numpy as np

def Bellmann(mdp, s, gamma, v):
    Q = {}
    for a in mdp.A(s):
        Q[a] = 0
        sp = mdp.next_state(s, a)
        r = mdp.reward[s][a]
        Q[a] += (r + gamma*v[sp])
    return Q

def value_iteration(mdp, gamma=.99, theta=0.0001, max_iters=10 ** 6, verbose=False):
    V = defaultdict(lambda: 0)  # value function
    for i in range(max_iters):
        delta = 0
        for s in np.arange(np.prod(mdp.original_shape))[mdp.terminal == 0]:
            v_ = V[s]
            V[s] = max(Bellmann(mdp, s, gamma, V).values())
            delta = max(delta, np.abs(v_ - V[s]))
        if verbose:
            print(i, delta)
        if delta < theta:
            break
    pi = values2policy(mdp, V, gamma)
    return pi, V

def values2policy(mdp, V, gamma):
    pi = {}
    for s in np.arange(np.prod(mdp.original_shape))[mdp.terminal == 0]:
        # Create the policy here. pi[s] = a is the action to be taken in state s.
        # You can use the qs_ helper function to simplify things and perhaps
        # re-use ideas from the dp.py problem from week 2.
        Q = Bellmann(mdp, s, gamma, V)
        pi[s] = max(Q, key=Q.get)
    return pi
