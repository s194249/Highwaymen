"""
Based on Tue Herlau value iteration agent
"""
#%%
import time
import numpy as np
import gym
import highway_env
from value_iteration import value_iteration
from highway_env.envs.common.finite_mdp import compute_ttc_grid

class ValueIterationAgent:
    def __init__(self, env, gamma=0.95, epsilon=0):
        self.env = env
        self.gamma = gamma
        self.epsilon = epsilon

    def get_policy(self):
        mdp = self.env.to_finite_mdp()
        mdp.terminal[mdp.state] = False
        mdp.A = lambda s: set(range(self.env.action_space.n)) # this could be done more sleekly but removes the need to change anything in the finite mdp file
        return value_iteration(mdp, self.gamma), mdp.state

    def pi(self, s):
        """ With probability (1-epsilon), the take optimal action as computed using value iteration
         With probability epsilon, take a random action. You can do this using return self.random_pi(s)
        """
        (policy, v), s = self.get_policy()

        if np.random.rand() < self.epsilon:
            return np.random.randint(0, 5)
        else:
            return policy[s], v

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    env = gym.make("highway-v0")  # "merge-v0", highway-v0, "roundabout-v0" "parking-v0"
    s = env.reset()
    done = False
    # also done in value iteration agent, so this should be deleted
    agent = ValueIterationAgent(env)
    while not done:
        grid = compute_ttc_grid(env, time_quantization=1/env.config["policy_frequency"], horizon=10)
        a, V = agent.pi(s)  # Your agent code here
        s, r, done, info = env.step(a)
        # print(r)
        for i in range(120):
            if i not in V:
                V[i] = 0
        a = np.asarray(list(V.items()))
        a = a[a[:, 0].argsort()][:,1].reshape((3,4,10))
        fig, ax = plt.subplots(figsize=((12,7)))
        plt.subplot(4,1,1)
        plt.imshow(env.render(mode="rgb_array"))
        plt.axis('off')
        plt.title(f'speed: {info["speed"]:.3}, reward: {r:.3}')
        plt.subplot(4,2,3)
        plt.title('Value function')
        plt.xticks(range(10))
        plt.imshow(a[0, :, :])
        plt.subplot(4,2,4)
        plt.xticks(range(10))
        plt.title('TTC grid')
        plt.imshow(grid[0, :, :])
        plt.subplot(4,2,5)
        plt.xticks(range(10))
        plt.imshow(a[1, :, :])
        plt.subplot(4,2,6)
        plt.xticks(range(10))
        plt.imshow(grid[1, :, :])
        plt.subplot(4,2,7)
        plt.xticks(range(10))
        plt.imshow(a[2, :, :])
        plt.subplot(4,2,8)
        plt.xticks(range(10))
        plt.imshow(grid[2, :, :])
        plt.tight_layout()
        plt.savefig(f'gifimg/{time.time()}.png')
        plt.show()
        env.render()

    env.close()
