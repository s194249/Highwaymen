# Example of the environment running
![gif](gifimg/gif.gif)

## Requirements
```
gym
highway-env
# finite mdp
git+https://github.com/eleurent/finite-mdp.git

pip install -r requirements.txt
```

## Scripts where we did the most work

```Q_agent.py``` - everything including and below the Qagent class, with some small modification to the above to make it work.

```Value_Iteration_Agent.py``` - all of it 

```value_iteration.py``` - all of it, although we were initially inpired by Tue's code

```plots.py``` and ```RUN.py``` - all, but this is mostly plotting and driver code

## Based on Highway-env
https://github.com/eleurent/highway-env
```
@misc{highway-env,
  author = {Leurent, Edouard},
  title = {An Environment for Autonomous Driving Decision-Making},
  year = {2018},
  publisher = {GitHub},
  journal = {GitHub repository},
  howpublished = {\url{https://github.com/eleurent/highway-env}},
}
```
