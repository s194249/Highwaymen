import gym
import highway_env
import numpy as np
from Value_Iteration_Agent import ValueIterationAgent
import matplotlib.pyplot as plt

env = gym.make("highway-v0")    

gamma = 0.95
epsilon = 0

parameters = [f'gamma: {gamma}', f'epsilon: {epsilon}']

num_experiment = 1000

rewards = [0] * num_experiment
steps = [0] * num_experiment
for i in range(num_experiment):
    print(f'Running episode: {i+1}/{num_experiment}')
    episode = 0
    done = False
    s = env.reset()
    agent = ValueIterationAgent(env, gamma=gamma, epsilon=epsilon)
    while not done:
        a = agent.pi(s) # Your agent code here
        s, r, done, info = env.step(a)
        rewards[i] += r
        episode += 1
        steps[i] += 1
        #env.render()

env.close()

np.savez(f'Value_Iteration_{num_experiment}episodes', rewards=rewards, steps=steps, parameters=parameters)

plt.figure()
plt.title('Accumulated Reward')
plt.bar(range(num_experiment), rewards)
plt.xticks(range(num_experiment))
plt.show()

plt.figure()
plt.title('rewards')
plt.hist(rewards)
plt.show()

plt.figure()
plt.title('No. of steps')
plt.bar(range(num_experiment), steps)
plt.xticks(range(num_experiment))
plt.show()