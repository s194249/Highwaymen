from collections import OrderedDict, defaultdict
import types
import inspect
import gym
import numpy as np

class defaultdict2(defaultdict):
    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError((key,))

        if isinstance(self.default_factory, types.FunctionType):
            nargs = len(inspect.getfullargspec(self.default_factory).args)
            self[key] = value = self.default_factory(key) if nargs == 1 else self.default_factory()
            return value
        else:
            return super().__missing__(key)

class TabularQ:
    """
    Tabular Q-values. This is a helper class for the Q-agent to store Q-values without too much hassle with
    state-dependent action spaces and so on.
    """
    def __init__(self, env):
        # This may need to be changed (s in P)
        qfun = lambda s: OrderedDict({a: 0 for a in (env.P[s] if hasattr(env, 'P') else range(5))})
        self.q_ = defaultdict2(lambda s: qfun(s))
        self.env = env
        
    def get_Qs(self, state):
        (actions, Qa) = zip(*self.q_[state].items())
        return tuple(actions), tuple(Qa)

    def get_optimal_action(self, state):
        actions, Qa = self.get_Qs(state)
        a_ = np.argmax(np.asarray(Qa) + np.random.rand(len(Qa)) * 1e-8)
        return actions[a_]

    def __getitem__(self, state_comma_action):
        s, a = state_comma_action
        return self.q_[s][a]

    def __setitem__(self, state_comma_action, q_value):
        s, a = state_comma_action
        self.q_[s][a] = q_value

    def items(self):  # not sure this is used
        raise Exception()
        return self.q_.items()

    def to_dict(self):
        # Convert to a regular dictionary
        d = {s: {a: Q for a, Q in Qs.items() } for s,Qs in self.q_.items()}
        return d

class QAgent:
    """
    Implement the Q-learning agent (SB18, Section 6.5)
    Note that the Q-datastructure already exist, as do helper functions useful to compute an epsilon-greedy policy
    (see TabularAgent class for more information)
    """
    def __init__(self, env, gamma=0.95, alpha=0.5, epsilon=0.2):
        self.env = env
        self.gamma = gamma
        self.alpha = alpha
        self.epsilon = epsilon
        self.Q = TabularQ(env)

    def pi(self, s, greedy=True):
        """ Implement epsilon-greedy exploration. Return random action with probability self.epsilon,
        else be greedy wrt. the Q-values. """
        if not greedy:
            return self.random_pi(s) if np.random.rand() < self.epsilon else self.Q.get_optimal_action(s)
        return self.Q.get_optimal_action(s)

    def random_pi(self, s):
        """ Generates a random action given s."""
        return np.random.randint(0, 5)
        
    def train(self, s, a, r, sp, done=False): 
        """
        Implement the Q-learning update rule, i.e. compute a* from the Q-values.
        As a hint, note that self.Q[sp,a] corresponds to q(s_{t+1}, a) and
        that what you need to update is self.Q[s, a] = ...

        You may want to look at self.Q.get_optimal_action(state) to compute a = argmax_a Q[s,a].
        """       
        a_s = self.Q.get_optimal_action(s)
        self.Q[s, a] = self.Q[s, a] + self.alpha * (r  + self.gamma * self.Q[sp, a_s] - self.Q[s, a])

# train
def train(mdp, agent, num_episodes=10_000):
    """trains agent for a number of episodes"""
    init_state = mdp.state
    for i in range(num_episodes):
        done = False
        if i > 0:
            s = init_state
            mdp.state = init_state
        else:
            s = mdp.state
        while not done:
            a = agent.pi(s, greedy=False)
            sp, r, done, info = mdp.step(a)
            agent.train(s, a, r, sp, done)
            s = sp

if __name__ == '__main__':
    import highway_env
    import matplotlib.pyplot as plt
    env = gym.make("highway-v0")
    env.config['screen_height'] = 150
    env.config['screen_width'] = 600
    env.config['collision_reward'] = -1

    gamma = 0.95
    alpha = 0.5
    epsilon = 0.2
    num_episodes = 500
    parameters = [f'gamma: {gamma}', f'alpha: {alpha}', f'epsilon: {epsilon}', f'num_episodes: {num_episodes}']

    num_experiment = 1000
    rewards = [0] * num_experiment
    steps = [0] * num_experiment
    for i in range(num_experiment):
        print(f'Running episode: {i+1}/{num_experiment}')
        episode = 0
        done = False
        env.reset()
        mdp = env.to_finite_mdp()
        agent = QAgent(mdp, gamma=gamma, alpha=alpha, epsilon=epsilon)
        while not done:
            s = mdp.state
            train(mdp, agent, num_episodes=500)
            a = agent.pi(s) # Your agent code here
            _, r, done, info = env.step(a)
            rewards[i] += r
            episode += 1
            steps[i] += 1
            #env.render()
            mdp = env.to_finite_mdp()
            agent = QAgent(mdp, gamma=gamma, alpha=alpha, epsilon=epsilon)


    np.savez(f'Q_agent_{num_experiment}episodes', rewards=rewards, steps=steps, parameters=parameters)
    env.close()

    plt.figure()
    plt.title('Accumulated Reward')
    plt.bar(range(num_experiment), rewards)
    plt.xticks(range(num_experiment))
    plt.show()

    plt.figure()
    plt.title('rewards')
    plt.hist(rewards)
    plt.show()

    plt.figure()
    plt.title('No. of steps')
    plt.bar(range(num_experiment), steps)
    plt.xticks(range(num_experiment))
    plt.show()


